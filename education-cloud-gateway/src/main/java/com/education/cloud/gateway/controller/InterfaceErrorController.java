package com.education.cloud.gateway.controller;

import com.education.cloud.util.base.Result;
import com.education.cloud.util.enums.ResultEnum;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description web错误 全局处理 未找到时返回json
 * @Date 2020/7/3
 * @Created by 67068
 */

@RestController
public class InterfaceErrorController implements ErrorController {

    @RequestMapping("/error")
    @ResponseStatus(HttpStatus.OK)
    public Result<String> errorJson(){
        RequestContext ctx = RequestContext.getCurrentContext();
        Throwable throwable = ctx.getThrowable();
        if(null != throwable && throwable instanceof ZuulException){
            ZuulException e = (ZuulException) ctx.getThrowable();
            return Result.error(e.nStatusCode, e.errorCause);
        }
        return Result.error(ResultEnum.ERROR);
    }


    /**
     * 出异常后进入该方法，交由下面的方法处理
     */
    @Override
    public String getErrorPath() {
        return "/error";
    }
}
