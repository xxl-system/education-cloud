package com.education.cloud.system.feign.interfaces;

import com.education.cloud.system.feign.qo.SysUserQO;
import com.education.cloud.system.feign.vo.SysUserVO;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.constant.ServiceConstant;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 后台用户信息
 *
 * @author wujing
 */
@FeignClient(name = ServiceConstant.SYSTEM_SERVICE,contextId = "sysUserClient")
public interface IFeignSysUser {


    @RequestMapping(value = "/feign/system/sysUser/listForPage")
    Page<SysUserVO> listForPage(@RequestBody SysUserQO qo);

    @RequestMapping(value = "/feign/system/sysUser/save")
    int save(@RequestBody SysUserQO qo);

    @RequestMapping(value = "/feign/system/sysUser/deleteById")
    int deleteById(@RequestBody Long id);

    @RequestMapping(value = "/feign/system/sysUser/updateById")
    int updateById(@RequestBody SysUserQO qo);

    @RequestMapping(value = "/feign/system/sysUser/getById")
    SysUserVO getById(@RequestBody Long id);

}
