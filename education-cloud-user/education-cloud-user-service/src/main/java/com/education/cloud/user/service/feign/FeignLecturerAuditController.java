package com.education.cloud.user.service.feign;

import com.education.cloud.user.feign.interfaces.IFeignLecturerAudit;
import com.education.cloud.user.feign.qo.LecturerAuditQO;
import com.education.cloud.user.feign.vo.LecturerAuditVO;
import com.education.cloud.user.service.feign.biz.FeignLecturerAuditBiz;
import com.education.cloud.util.base.Page;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.education.cloud.util.base.BaseController;

/**
 * 讲师信息-审核
 *
 * @author wujing
 */

@Api(value = "讲师信息-审核", tags = "讲师信息-审核")
@RestController
public class FeignLecturerAuditController extends BaseController implements IFeignLecturerAudit {

    @Autowired
    private FeignLecturerAuditBiz biz;

    @Override
    public Page<LecturerAuditVO> listForPage(@RequestBody LecturerAuditQO qo) {
        return biz.listForPage(qo);
    }

    @Override
    public int save(@RequestBody LecturerAuditQO qo) {
        return biz.save(qo);
    }

    @Override
    public int deleteById(@PathVariable(value = "id") Long id) {
        return biz.deleteById(id);
    }

    @Override
    public int updateById(@RequestBody LecturerAuditQO qo) {
        return biz.updateById(qo);
    }

    @Override
    public LecturerAuditVO getById(@PathVariable(value = "id") Long id) {
        return biz.getById(id);
    }

    @Override
    public int audit(@RequestBody LecturerAuditQO qo) {
        return biz.audit(qo);
    }

    @Override
    public LecturerAuditVO checkUserAndLecturer(@RequestBody LecturerAuditQO qo) {
        return biz.checkUserAndLecturer(qo);
    }

}
